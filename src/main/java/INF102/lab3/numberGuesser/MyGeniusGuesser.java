package INF102.lab3.numberGuesser;


public class MyGeniusGuesser implements IGuesser {
private int lowerbound;
private int upperbound;
private int midOfList;
private int guessCount;

	@Override
    public int findNumber(RandomNumber number) {
        if (guessCount == 0) {
            lowerbound = number.getLowerbound();
            upperbound = number.getUpperbound();
        } //Hvis det er første gang metoden kjøres (altså ingen gjetninger
		midOfList = (lowerbound + upperbound) /2; //Finner midterste tallet av upperbound og lowerbound (høyeste og laveste tall)
        int guessFeedback = number.guess(midOfList); //Gjett midterste tall
        guessCount = number.getGuessCount(); //Hent antall gjetninger
        if (guessFeedback == 0) { //Hvis midterste tall er riktig
            guessCount = 0; //Sett antall gjetninger til 0
            return midOfList; //Returner midterste tall
        } else if (guessFeedback == -1) { //Hvis midterste tall er for lavt
            lowerbound = midOfList; //Sett lowerbound til midterste tall
            return findNumber(number); //Kjør metoden på nytt
        } else { //Hvis midterste tall er for høyt
            upperbound = midOfList; //Sett upperbound til midterste tall
            return findNumber(number); //Kjør metoden på nytt
        }

    }

}
